package Pages;

import Services.User;
import Services.Utils;
import Services.Wait;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.io.IOException;
import java.util.List;

public class CheckOutPage {
    private WebDriver driver;
    private Wait wait;
    private Utils utils;

    public CheckOutPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new Wait(driver, 10);
        this.utils = new Utils(driver);
    }

    @FindBy(id = "checkout3-accordion")
    private WebElement checkoutPageForm;

    @FindBy(id = "signin3-form-email")
    private WebElement email;

    @FindBy(id = "billinginfo3-form-fullname")
    private WebElement fullName;

    @FindBy(id = "billinginfo3_form_countryiso2_chosen")
    private WebElement countryChosen;

    @FindBy(xpath = ".//*[@id='billinginfo3_form_phone_code_chosen']/a")
    private WebElement phoneCodeChosen;

    @FindBy(id = "billinginfo3-form-phone")
    private WebElement phone;

    @FindBy(id = "billinginfo3-form-postalcode")
    private WebElement postalCode;

    @FindBy(id = "billinginfo3-form-cityname")
    private WebElement cityName;

    @FindBy(id = "billinginfo3-form-submit")
    private WebElement submitButton;

    @FindBy(id="checkout-payment-buy-PayPal")
    private WebElement viaPayPalButton;

    private By countryListXpath = By.xpath(".//*[@id='billinginfo3_form_countryiso2_chosen']/div/ul");
    private By phoneListXpath = By.xpath(".//*[@id='billinginfo3_form_phone_code_chosen']/div/ul");

    public WebElement getViaPayPalButton() {
        return viaPayPalButton;
    }

    private void chooseEl(User user, By xpath){
        List<WebElement> list = driver.findElements(xpath);
        String xPath = ".//li[contains(@class, 'iti-flag " + user.getCountry().toLowerCase() + "')]";
        for (WebElement el : list) {
            el.findElement(By.xpath(xPath)).click();
        }
    }

    public void registerBuyer(User user) throws IOException {
        wait.isVisible(checkoutPageForm);

        wait.isClickable(email);
        email.sendKeys(user.getEmail());

        wait.isVisible(fullName);
        fullName.sendKeys(user.getFullName());

        utils.clickOnElement(countryChosen);
        chooseEl(user, countryListXpath);

        utils.clickOnElement(phoneCodeChosen);
        chooseEl(user, phoneListXpath);

        wait.isVisible(phone);
        phone.sendKeys(user.getPhone());

        wait.isVisible(postalCode);
        postalCode.sendKeys(user.getPostalCode());

        wait.isVisible(cityName);

        utils.clickOnElement(submitButton);

        wait.isVisible(viaPayPalButton);
        wait.isClickable(viaPayPalButton);
    }
}