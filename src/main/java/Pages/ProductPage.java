package Pages;

import Services.Utils;
import Services.Wait;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class ProductPage {
    private WebDriver driver;
    private Wait wait;
    private Utils utils;

    public ProductPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new Wait(driver, 10);
        this.utils = new Utils(driver);
    }

    @FindBy(id = "menu-favorites")
    private WebElement heartIcon;

    @FindBy(xpath = ".//button[contains(@class, 'button-add-to-cart')]")
    private WebElement addToCartButton;

    @FindBy(id = "cart-summary-checkout")
    private WebElement checkOutButton;

    @FindBy(id = "checkout3-accordion")
    private WebElement checkoutPageForm;

    public WebElement getCheckoutPageForm() {
        return checkoutPageForm;
    }

    public void addProductToCart(){
        wait.isVisible(heartIcon);

        Actions actions = new Actions(driver);
        actions.moveToElement(addToCartButton).build().perform();
        utils.clickOnElement(addToCartButton);

        String parentHandle = driver.getWindowHandle();

        if(driver.getWindowHandles().size() > 1){
            for (String handle : driver.getWindowHandles()) {
                if(!handle.equals(parentHandle)){
                    driver.switchTo().window(handle);
                }
            }
        }

        utils.clickOnElement(checkOutButton);
    }
}