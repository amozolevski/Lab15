package Pages;

import org.openqa.selenium.WebDriver;

public class MainPage {
    private WebDriver driver;

    public MainPage(WebDriver driver) {
        this.driver = driver;
    }

    public void chooseProduct(){
        driver.get("https://www.templatemonster.com/drupal-themes/treveler-travel-blog-premium-drupal-template-65735.html");
    }
}