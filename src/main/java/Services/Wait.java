package Services;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Wait {
    private final WebDriverWait wait;

    public Wait(WebDriver driver, long sec) {
        this.wait = new WebDriverWait(driver, sec);
    }

    public void isClickable(WebElement element){
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public void isVisible(WebElement element){
        wait.until((WebDriver webdriver) -> element.isDisplayed());
    }

}
