package Services;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Utils {
    private WebDriver driver;
    private Wait wait;

    public Utils(WebDriver driver) {
        this.driver = driver;
        wait = new Wait(driver, 10);
    }

    public void clickOnElement(WebElement button){
        wait.isClickable(button);
        button.click();
    }

    public boolean isElementDisplayed(WebElement element){
        return element.isDisplayed();
    }

    public void takeScreenShot() throws IOException {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat formater = new SimpleDateFormat("ddMMyyyy_hhmm");

        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File("target/screenshots/screenshot_" + formater.format(calendar.getTime())+ ".png"));
    }

}
