package Services;

import java.io.*;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class User {

    private Map<String, String> propMap = new HashMap<>();

    public User(String country) throws FileNotFoundException {
        getUserProp(country);
    }

    public String getEmail() {
        return randomString() + propMap.get("email");
    }

    public String getFullName() {
        return propMap.get("fullname");
    }

    public String getCountry() {
        return propMap.get("country");
    }

    public String getPhone() {
        return propMap.get("phone");
    }

    public String getPostalCode() {
        return propMap.get("postalcode");
    }

    public String randomString() {
        String currentTime = Long.toString(System.currentTimeMillis());
        return "www" + currentTime;
    }

    public void getUserProp(String country) throws FileNotFoundException {
        try {
            File file = new File("src/test/java/resources/" + country + ".properties");
            InputStreamReader fileReader = new InputStreamReader(new FileInputStream(file), "UTF8");
            Properties properties = new Properties();
            properties.load(fileReader);
            fileReader.close();

            Enumeration enuKeys = properties.keys();
            while (enuKeys.hasMoreElements()) {
                String key = (String) enuKeys.nextElement();
                String value = properties.getProperty(key);
                propMap.put(key, value);
            }
        } catch (FileNotFoundException e) {
            System.out.println("Exception: " + e);
        } catch (IOException e) {
        }
    }
}