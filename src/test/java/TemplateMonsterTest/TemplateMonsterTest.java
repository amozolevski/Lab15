package TemplateMonsterTest;

import Pages.CheckOutPage;
import Pages.MainPage;
import Pages.ProductPage;
import Services.User;
import Services.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class TemplateMonsterTest {
    private WebDriver driver;
    private Utils utils;
    private MainPage mainPage;
    private ProductPage productPage;
    private CheckOutPage checkOutPage;

    @BeforeMethod
    void beforeTest() throws MalformedURLException {
        driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"),
                                    DesiredCapabilities.chrome());
        driver.get("https://www.templatemonster.com/");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//        driver.manage().window().maximize();
        utils = new Utils(driver);
        mainPage = PageFactory.initElements(driver, MainPage.class);
        productPage = PageFactory.initElements(driver, ProductPage.class);
        checkOutPage = PageFactory.initElements(driver, CheckOutPage.class);
    }

    @BeforeMethod
    void escClick(){
        driver.findElement(By.xpath(".//body")).sendKeys(Keys.ESCAPE);
    }

    @Test()
    void addProductToCartTestCase(){
        mainPage.chooseProduct();
        productPage.addProductToCart();
        Assert.assertTrue(utils.isElementDisplayed(productPage.getCheckoutPageForm()), "it isn't checkOutPage");
    }

    @DataProvider(name = "chooseUser")
    public Object[][] chooseUser(){
        return new String[][]{
                            {"UA"},
                            {"US"}
        };
    }

    @Test(dataProvider = "chooseUser")
    void CheckOutTestCase(String userCountry) throws IOException {
        addProductToCartTestCase();
        checkOutPage.registerBuyer(new User(userCountry));
        Assert.assertTrue(utils.isElementDisplayed(checkOutPage.getViaPayPalButton()), "PayPal button isn't present");
    }

    @AfterMethod()
    void afterTest() throws IOException {
        utils.takeScreenShot();
        driver.quit();
    }
}